libhtml-wikiconverter-markdown-perl (0.06-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 13 Dec 2022 02:10:55 +0000

libhtml-wikiconverter-markdown-perl (0.06-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jose Luis Rivas from Uploaders. Thanks for your work!
  * Add Testsuite declaration for autopkgtest-pkg-perl.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Add missing runtime dependencies on libparams-validate-perl et al.
    (Closes: #896547)
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.4
  * Declare that the package does not need (fake)root to build
  * Add myself to Uploaders

 -- Niko Tyni <ntyni@debian.org>  Sun, 22 Apr 2018 13:45:05 +0300

libhtml-wikiconverter-markdown-perl (0.06-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Use dh(1) and debhelper 8.
  * Add upstream copyright year, refresh license stanzas.
  * Update (versioned) (build) dependencies.
  * Drop Breaks/Replaces on ancient libhtml-wikiconverter-perl version.
  * Drop debian/clean, not needed.

 -- gregor herrmann <gregoa@debian.org>  Tue, 12 Aug 2014 22:35:04 +0200

libhtml-wikiconverter-markdown-perl (0.05-1) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * Replace "MoinMoin" with "Markdown" in short description, thanks to Olivier
    Schwander for catching this copy/paste error (closes: #508083).

  [ David Bremner ]
  * New upstream release 0.03. Fixes FTBFS. Closes: bug#490697.

  [ Jonas Smedegaard ]
  * Add DEB_MAINTAINER_MODE in debian/rules (thanks to Romain Beauxis).
  * Tighten libhtml-wikiconverter-perl dependency to at least 0.63 (to
    match updated upstream MakeMaker hint).
  * Update CDBS snippets:
    + Add new local package-relations.mk to resolve, cleanup and apply
      CDBS-declared (build-)dependencies.
    + Update copyright-check.mk to closer match new proposed copyright
      format.
    + Correct and update copyright hints of the snippets themselves
    + Simplify internal variables
    + Ignore no files by default in copyright-check.mk
    + Update README.cdbs-tweaks.
  * Add TODO.source with note on need to document CDBS use.
  * Update MD5 checksum hint for tarball of new upstream release.
  * Update debian/copyright and copyright hints:
    + Add info on CDBS snippets (no new owners or licenses)
    + Rewrite to conform to version 413 of new copyright-file format
    + Withdraw passing packaging ownership to Perl group (uncertainty of
      legal problems of group ownership)
  * Semi-auto-update debian/control to update dependencies:
      DEB_MAINTAINER_MODE=1 fakeroot debian/rules clean
  * Remove myself as uploader - replacing with Jose Luis Rivas Contreras
    who clearly took over.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Jose Luis Rivas ]
  * New upstream release 0.05
  * Deleted patches for ikiwiki migrations.
  * Removed quilt, devscripts, patchutils and dh-buildinfo from B-D and
    removed debian/README.source.
  * Updated version of libhtml-wikiconverter-perl to >= 0.67.
  * debian/control: versioned debhelper to >= 7
  * Updated debian/rules to a simple use of CDBS.
  * Deleted custom rules for CDBS and debian/README.cdbs-tweaks.
  * Deleted use of debian/control.in
  * Removed auto-svn-upgrade on debian/watch.
  * Removed debian/TODO.source, no longer needed.

  [ gregor herrmann ]
  * Set Standards-Version to 3.9.1; replace Conflicts with Breaks.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"
  * Apply wrap-and-sort
  * Fix CDBS warning "perlmodule.mk is deprecated - please use
    perl-makemaker.mk instead"
  * Switch to source format "3.0 (quilt)"
  * Add Makefile.old to debian/clean.
  * Fix lintian warning copyright-refers-to-symlink-license
  * Set Homepage to https://metacpan.org/release/HTML-WikiConverter-Markdown
    (old homepage now redirects to a link spammer)
  * Update watch and copyright file to use metacpan.org, too.
  * Bump Standards-Version to 3.9.5 (no further changes)
  * Fix dpkg-gencontrol warning "unknown substitution variable
    ${cdbs:Depends}"

 -- Axel Beckert <abe@debian.org>  Wed, 12 Mar 2014 20:56:40 +0100

libhtml-wikiconverter-markdown-perl (0.02-6) unstable; urgency=high

  * Disable tests.  Due to unknown reasons test 4 and 5 started failing
    between 20080628 (when packaged) and 20080711 (see bug#490349).
  * Really conflict and replace old libhtml-wikiconverter-perl as
    promised (although mistyped) in 0.02-4).
  * Set urgency=high as this fixes a FTBFS with only minor additional
    changes.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 13 Jul 2008 17:41:57 +0200

libhtml-wikiconverter-markdown-perl (0.02-5) UNRELEASED; urgency=low

  * Depend on ${misc:Depends}.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 28 Jun 2008 22:51:36 +0200

libhtml-wikiconverter-markdown-perl (0.02-4) unstable; urgency=medium

  * Provide and replace old libhtml-wikiconverter-perl, to ease
    transition to split packages.
  * Keep urgency=medium.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 28 Jun 2008 22:50:33 +0200

libhtml-wikiconverter-markdown-perl (0.02-3) unstable; urgency=medium

  * Fix 0.02-2 changelog entry (separate who changed what).
  * Set urgency=medium as 0.02-2 included a FTBFS bugfix (similar to
    bug#487066).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 28 Jun 2008 20:17:13 +0200

libhtml-wikiconverter-markdown-perl (0.02-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: fix short description - this package is about Markdown
    and not about MoinMoin (closes: #479387).

  [ Jonas Smedegaard ]
  * Update local cdbs tweaks:
    + Various updates to copyright-check.mk, most importantly relaxing
      to only warn by default.  Closes: bug#487064, thanks to Lucas
      Nussbaum.
    + Drop wget options broken with recent versions of wget in
      update-tarball.mk.
    + Update dependency cleanup to strip cdbs 0.4.27 (not 0.4.27-1).
  * Update debian/copyright-hints.
  * Bump debhelper compatibility level to 6.
  * Semi-auto-update debian/control to update build-dependencies:
      DEB_AUTO_UPDATE_DEBIAN_CONTROL=yes fakeroot debian/rules clean

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 28 Jun 2008 19:57:22 +0200

libhtml-wikiconverter-markdown-perl (0.02-1) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Initial official release. Closes: bug#448912.
  * Pass over maintenance of the package to the Perl group:
    + Change Maintainer, and add myself to Uploaders.
    + Add Perl group as copyright holder for 2008.
  * Update cdbs routines.
  * Bump up standards-version (no changes needed).
  * Add Vcs-* fields to debian/control.
  * Change debian/watch to use svn-upgrade (not uupdate).

  [ Martín Ferrari ]
  * Updated debian/watch to use by-dist.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 19 Mar 2008 01:42:39 +0100

libhtml-wikiconverter-markdown-perl (0.02-0~0jones1) jones; urgency=low

  * Initial unofficial build.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 01 Oct 2007 18:33:01 +0200
